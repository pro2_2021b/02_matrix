package experiments;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SaveMatrix
{
    public static void main(String[] args) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String heightInput = bufferedReader.readLine();
        String widthInput = bufferedReader.readLine();

        Path path = Paths.get(System.getProperty("user.dir"),"matrix.bin");
        FileOutputStream fileOutputStream = new FileOutputStream(path.toString());
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);


        int height = Integer.parseInt(heightInput);
        int width = Integer.parseInt(widthInput);

        for(int row=0; row < height; row++)
        {
            String line = bufferedReader.readLine();
            String[] numbers = line.split(" ");

            for(int column = 0; column < width; column++)
            {
                int number = Integer.parseInt(numbers[column]);
                dataOutputStream.writeInt(number);
            }
        }

        dataOutputStream.close();



    }
}
