package experiments;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SaveMatrixToCSV
{
    public static void main(String[] args) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String heightInput = bufferedReader.readLine();
        String widthInput = bufferedReader.readLine();

        Path path = Paths.get(System.getProperty("user.dir"),"matrix.csv");
        FileOutputStream fileOutputStream = new FileOutputStream(path.toString());

        OutputStreamWriter outputStreamWriter =
                new OutputStreamWriter(fileOutputStream, Charset.forName("UTF-8") );


        int height = Integer.parseInt(heightInput);
        int width = Integer.parseInt(widthInput);

        for(int row=0; row < height; row++)
        {
            String line = bufferedReader.readLine();
            String[] numbers = line.split(" ");

            for(int column = 0; column < width; column++)
            {
                if(column > 0)
                {
                    outputStreamWriter.write(";");
                }
                int number = Integer.parseInt(numbers[column]);
                outputStreamWriter.write(Integer.toString(number));
            }

            if(row < height - 1)
            {
                outputStreamWriter.write("\n");
            }
        }

        outputStreamWriter.close();
    }
}
